package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.entity.Customer;
import com.example.demo.entity.TakeQ;

@Repository
@CrossOrigin(origins = "http://localhost:3000")
public interface TakeQRepository extends JpaRepository<TakeQ, Long> {

//	@Query(value = "SELECT *" + " FROM Takeq " + "where " + "customer_id = ?1 ")
//	TakeQ getStatusByCustomerId(long customerId);
}