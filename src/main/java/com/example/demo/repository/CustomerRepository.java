package com.example.demo.repository;

import javax.transaction.Transactional;

import org.jboss.logging.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.entity.Customer;
import com.example.demo.entity.LoginModel;

@Repository
@CrossOrigin(origins = "http://localhost:3000")
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	
	@Query(value = "SELECT c" + " FROM Customer c " + " where " + "c.email = ?1 " + "AND "
			+ "c.password = ?2 ")
	Customer login(String email, String password);
}