package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Q;
import com.example.demo.entity.Staff;

@Repository
public interface QRepository extends JpaRepository<Q, Long> {

}