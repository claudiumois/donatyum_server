package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.entity.Appointment;
import com.example.demo.entity.AppointmentResponse;
import com.example.demo.entity.Customer;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long>{

}