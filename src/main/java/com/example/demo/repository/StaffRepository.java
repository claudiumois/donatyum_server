package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Staff;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Long> {

	@Query(value = "SELECT s" + " FROM Staff s " + " where " + "s.email = ?1 " + "AND "
			+ "s.password = ?2 ")
	Staff login(String email, String password);
	
	@Query(value = "SELECT s" + " FROM Staff s " + " where " + "s.staffOcupationId = ?1 ")
	List<Staff> getAllByOcupationId(int ocupation);
	
	
}