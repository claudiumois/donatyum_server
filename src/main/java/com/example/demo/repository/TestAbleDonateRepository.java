package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Staff;
import com.example.demo.entity.TestAbleDonate;

@Repository
public interface TestAbleDonateRepository extends JpaRepository<TestAbleDonate, Long> {

}