package com.example.demo.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Q")
public class Q {/* Chestionar */

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "question")
	private String question;

	@Column(name = "answer_a")
	private String answer_a;

	@Column(name = "answer_b")
	private String answer_b;

	@Column(name = "answerCustomer")
	private String answerCustomer;

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties
	@JsonIgnore
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = true)
	private Customer customer;

	public Q() {
	}

	public Q(long id, String question, String answer_a, String answer_b, String answerCustomer,
			Customer customer) {
		super();
		this.id = id;
		this.question = question;
		this.answer_a = answer_a;
		this.answer_b = answer_b;
		this.answerCustomer = answerCustomer;
		this.customer = customer;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer_a() {
		return answer_a;
	}

	public void setAnswer_a(String answer_a) {
		this.answer_a = answer_a;
	}

	public String getAnswer_b() {
		return answer_b;
	}

	public void setAnswer_b(String answer_b) {
		this.answer_b = answer_b;
	}

	public String getAnswerCustomer() {
		return answerCustomer;
	}

	public void setAnswerCustomer(String answerCustomer) {
		this.answerCustomer = answerCustomer;
	}

//	public long getCustomerId() {
//		return customerId;
//	}
//
//	public void setCustomerId(long customerId) {
//		this.customerId = customerId;
//	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	

}
