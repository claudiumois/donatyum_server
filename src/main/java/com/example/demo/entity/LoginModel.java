package com.example.demo.entity;

public class LoginModel {
	
	private String email;
	
	private String password;
	
	private long userId;
	
	private long userOcupationId;
	
	public LoginModel() {
	}

	public LoginModel(String email, String password, long userId, long userOcupationId) {
		super();
		this.email = email;
		this.password = password;
		this.userId = userId;
		this.userOcupationId = userOcupationId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUserOcupationId() {
		return userOcupationId;
	}

	public void setUserOcupationId(long userOcupationId) {
		this.userOcupationId = userOcupationId;
	}

	
}
