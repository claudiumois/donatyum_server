package com.example.demo.entity;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "blood")
public class Blood {/* Appointment */

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "typeblood")
	private String typeblood;

	@Column(name = "quantity")
	private float quantity;

	@Column(name = "status")
	private int status;

	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties
	@JsonIgnore
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = true)
	private Customer customer;

	public Blood() {
	}

	public Blood(long id, String typeblood, float quantity, int status, Customer customer) {
		super();
		this.id = id;
		this.typeblood = typeblood;
		this.quantity = quantity;
		this.status = status;
		this.customer = customer;
	}

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTypeblood() {
		return typeblood;
	}

	public void setTypeblood(String typeblood) {
		this.typeblood = typeblood;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
