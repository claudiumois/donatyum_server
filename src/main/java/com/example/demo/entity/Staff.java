package com.example.demo.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Staff")
public class Staff {

	@Column(name = "idStaff")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idStaff;

	@Column(name = "userName")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "email")
	private String email;

	@Column(name = "phone")
	private int phone;

	@Column(name = "address")
	private String address;

	@Column(name = "salary")
	private double salary;

	@Column(name = "staffOcupationId")
	private int staffOcupationId;

	@Column(name = "birthDate")
	private Date birthDate;

	@Column(name = "gender")
	private String gender;

	public Staff() {
	}

	public Staff(long idStaff, String userName, String password, String firstName, String lastName, String email,
			int phone, String address, double salary, int staffOcupationId, Date birthDate, String gender) {
		super();
		this.idStaff = idStaff;
		this.userName = userName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.salary = salary;
		this.staffOcupationId = staffOcupationId;
		this.birthDate = birthDate;
		this.gender = gender;
	}

	public long getIdStaff() {
		return idStaff;
	}

	public void setIdStaff(long idStaff) {
		this.idStaff = idStaff;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public int getStaffOcupationId() {
		return staffOcupationId;
	}

	public void setStaffOcupationId(int staffOcupationId) {
		this.staffOcupationId = staffOcupationId;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Staff [idStaff=" + idStaff + ", userName=" + userName + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + ", email=" + email + ", phone=" + phone + ", address="
				+ address + ", salary=" + salary + ", staffOcupationId=" + staffOcupationId + ", birthDate=" + birthDate
				+ ", gender=" + gender + "]";
	}
}
