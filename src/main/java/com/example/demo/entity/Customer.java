package com.example.demo.entity;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.json.JSONPropertyIgnore;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "customer")
public class Customer {/* DONATOR */

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "userName")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "email")
	private String email;

	@Column(name = "phone")
	private int phone;

	@Column(name = "address")
	private String address;

	@Column(name = "birthDate")
	private Date birthDate;

	@Column(name = "gender")
	private String gender;
	
	@OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
	@JsonIgnoreProperties
	private Appointment appointment;
	
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
	@JsonIgnoreProperties
	private List<Q> allQ;
	
	@OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
	@JsonIgnoreProperties
	private TestAbleDonate testAbleDonate;
	
	@OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
	@JsonIgnoreProperties
	private TestBlood testBlood;
	
	@OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
	@JsonIgnoreProperties
	private Blood blood;

	

	public Customer() {
	}

	public Customer(long id, String userName, String password, String firstName, String lastName, String email,
			int phone, String address, Date birthDate, String gender) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.birthDate = birthDate;
		this.gender = gender;
	}
	
	
	
	public Blood getBlood() {
		return blood;
	}

	public void setBlood(Blood blood) {
		this.blood = blood;
	}

	public TestBlood getTestBlood() {
		return testBlood;
	}

	public void setTestBlood(TestBlood testBlood) {
		this.testBlood = testBlood;
	}

	public long getIdCustomer() {
		return id;
	}

	public void setIdCustomer(long idCustomer) {
		this.id = idCustomer;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public List<Q> getAllQ() {
		return allQ;
	}

	public void setAllQ(List<Q> allQ) {
		this.allQ = allQ;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public TestAbleDonate getTestAbleDonate() {
		return testAbleDonate;
	}

	public void setTestAbleDonate(TestAbleDonate testAbleDonate) {
		this.testAbleDonate = testAbleDonate;
	}

}
