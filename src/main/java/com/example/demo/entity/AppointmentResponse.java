package com.example.demo.entity;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AppointmentResponse {
	
	private Customer customer;
	
	private Appointment appointment;

	public AppointmentResponse() {
		
	}

	public AppointmentResponse(int idAppointment, String first_name, String last_name) {
		this.appointment.setIdAppointment(idAppointment);
		this.customer.setFirstName(first_name);
		this.customer.setLastName(last_name);
	}



	

	

	

	
	

}
