package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "testBlood")
public class TestBlood {

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "hiv")
	private boolean hiv;

	@Column(name = "htlv")
	private boolean htlv;

	@Column(name = "hbs")
	private boolean hbs;

	@Column(name = "hcv")
	private boolean hcv;

	@Column(name = "alt")
	private boolean alt;

	
	@Column(name = "got")
	private boolean got;

	@Column(name = "gpt")
	private boolean gpt;

	@Column(name = "status")
	private int status;

	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties
	@JsonIgnore
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = true)
	private Customer customer;

	public TestBlood() {
	}

	public TestBlood(long id, boolean hiv, boolean htlv, boolean hbs, boolean hcv, boolean alt, boolean got,
			boolean gpt, int status, Customer customer) {
		super();
		this.id = id;
		this.hiv = hiv;
		this.htlv = htlv;
		this.hbs = hbs;
		this.hcv = hcv;
		this.alt = alt;
		this.got = got;
		this.gpt = gpt;
		this.status = status;
		this.customer = customer;
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isHiv() {
		return hiv;
	}

	public void setHiv(boolean hiv) {
		this.hiv = hiv;
	}

	public boolean isHtlv() {
		return htlv;
	}

	public void setHtlv(boolean htlv) {
		this.htlv = htlv;
	}

	public boolean isHbs() {
		return hbs;
	}

	public void setHbs(boolean hbs) {
		this.hbs = hbs;
	}

	public boolean isHcv() {
		return hcv;
	}

	public void setHcv(boolean hcv) {
		this.hcv = hcv;
	}

	public boolean isAlt() {
		return alt;
	}

	public void setAlt(boolean alt) {
		this.alt = alt;
	}

	public boolean isGot() {
		return got;
	}

	public void setGot(boolean got) {
		this.got = got;
	}

	public boolean isGpt() {
		return gpt;
	}

	public void setGpt(boolean gpt) {
		this.gpt = gpt;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
