package com.example.demo.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "StaffOcupation")
public class StaffOcupation {

	@Column(name = "idStaffOcupation")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idStaffOcupation;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;

	@Column(name = "ocupationId")
	private long ocupationId;

	public StaffOcupation() {
	}

	public StaffOcupation(long idStaffOcupation, String name, String description, long ocupationId) {
		super();
		this.idStaffOcupation = idStaffOcupation;
		this.name = name;
		this.description = description;
		this.ocupationId = ocupationId;
	}

	public long getIdStaffOcupation() {
		return idStaffOcupation;
	}

	public void setIdStaffOcupation(long idStaffOcupation) {
		this.idStaffOcupation = idStaffOcupation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getOcupationId() {
		return ocupationId;
	}

	public void setOcupationId(long ocupationId) {
		this.ocupationId = ocupationId;
	}

	
}
