package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "testAptDonate")
public class TestAbleDonate {

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "typeBlood")
	private String typeBlood;/* Tip de sange */

	@Column(name = "tension")
	private float tension;/* Tensiune */

	@Column(name = "bodyTemperature")
	private float bodyTemperature;/* Temperatura corporala */

	@Column(name = "height")
	private float height;

	@Column(name = "weight")
	private float weight;/* greutate */

	@Column(name = "hemoglobina")
	private float hemoglobina;

	@Column(name = "glicemie")
	private float glicemie;

	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties
	@JsonIgnore
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = true)
	private Customer customer;

	public TestAbleDonate() {
	}

	public TestAbleDonate(long id, String typeBlood, float tension, float bodyTemperature, float height, float weight,
			float hemoglobina, float glicemie, Customer customer) {
		super();
		this.id = id;
		this.typeBlood = typeBlood;
		this.tension = tension;
		this.bodyTemperature = bodyTemperature;
		this.height = height;
		this.weight = weight;
		this.hemoglobina = hemoglobina;
		this.glicemie = glicemie;
		this.customer = customer;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTypeBlood() {
		return typeBlood;
	}

	public void setTypeBlood(String typeBlood) {
		this.typeBlood = typeBlood;
	}

	public float getTension() {
		return tension;
	}

	public void setTension(float tension) {
		this.tension = tension;
	}

	

	public float getBodyTemperature() {
		return bodyTemperature;
	}

	public void setBodyTemperature(float bodyTemperature) {
		this.bodyTemperature = bodyTemperature;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getHemoglobina() {
		return hemoglobina;
	}

	public void setHemoglobina(float hemoglobina) {
		this.hemoglobina = hemoglobina;
	}

	public float getGlicemie() {
		return glicemie;
	}

	public void setGlicemie(float glicemie) {
		this.glicemie = glicemie;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
