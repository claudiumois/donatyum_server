package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TakeQ")
public class TakeQ {/* Tabela care verifica daca Clientul a facut sau nu Chestionarul */

	@Column(name = "idTakeQ")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idTakeQ;

	@Column(name = "customerId")
	private long customerId;

	@Column(name = "statusTake")
	private int statusTake;

	public TakeQ() {
	}

	public TakeQ(long idTakeQ, long customerId, int status) {
		super();
		this.idTakeQ = idTakeQ;
		this.customerId = customerId;
		this.statusTake = status;
	}

	public long getIdTakeQ() {
		return idTakeQ;
	}

	public void setIdTakeQ(long idTakeQ) {
		this.idTakeQ = idTakeQ;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public int getStatus() {
		return statusTake;
	}

	public void setStatus(int status) {
		this.statusTake = status;
	}

}
