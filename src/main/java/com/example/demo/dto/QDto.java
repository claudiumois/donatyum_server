package com.example.demo.dto;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class QDto {
	
	@JsonProperty( value = "id")
	private long id;

	@Column(name = "question")
	private String question;

	@Column(name = "answer_a")
	private String answer_a;

	@Column(name = "answer_b")
	private String answer_b;

	@Column(name = "answerCustomer")
	private String answerCustomer;
	
	@JsonProperty( value = "idCustomer")
	private long idCustomer;

	public QDto() {
	}

	public QDto(long id, String question, String answer_a, String answer_b, String answerCustomer, long idCustomer) {
		this.id = id;
		this.question = question;
		this.answer_a = answer_a;
		this.answer_b = answer_b;
		this.answerCustomer = answerCustomer;
		this.idCustomer = idCustomer;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer_a() {
		return answer_a;
	}

	public void setAnswer_a(String answer_a) {
		this.answer_a = answer_a;
	}

	public String getAnswer_b() {
		return answer_b;
	}

	public void setAnswer_b(String answer_b) {
		this.answer_b = answer_b;
	}

	public String getAnswerCustomer() {
		return answerCustomer;
	}

	public void setAnswerCustomer(String answerCustomer) {
		this.answerCustomer = answerCustomer;
	}

	public long getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(long idCustomer) {
		this.idCustomer = idCustomer;
	}

}
