package com.example.demo.dto;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AppointmentDto {
	
	@JsonProperty( value = "id")
	private long id;

	@JsonProperty( value = "firstName")
	private String firstName;
	
	@JsonProperty( value = "lastName")
	private String lastName;
	
	@JsonProperty( value = "time")
	private Time time;
	
	@JsonProperty( value = "date")
	private Date date;
	
	@JsonProperty( value = "idCustomer")
	private long idCustomer;
	
	@JsonProperty( value = "status")
	private int status;

	public AppointmentDto() {
	}

	public AppointmentDto(long id, String firstName, String lastName, Time time, Date date, long idCustomer,
			int status) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.time = time;
		this.date = date;
		this.idCustomer = idCustomer;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(long idCustomer) {
		this.idCustomer = idCustomer;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	

}
