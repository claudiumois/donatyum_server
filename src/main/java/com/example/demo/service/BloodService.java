package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Blood;
import com.example.demo.entity.TestBlood;
import com.example.demo.repository.BloodRepository;
import com.example.demo.repository.TestBloodRepository;


@Service
@Transactional
public class BloodService {

	@Autowired
	private BloodRepository bloodRepository;

	public List<Blood> findAll() {
		return bloodRepository.findAll();
	}

	public Optional<Blood> findById(Long id) {
		return bloodRepository.findById(id);
	}

	public Blood save(Blood blood) {
		return bloodRepository.save(blood);
	}

	public void deleteById(Long id) {
		bloodRepository.deleteById(id);
	}

	public Blood getBloodById(Long id) {
		Optional<Blood> blood = bloodRepository.findById(id);

		if (blood.isPresent()) {
			return blood.get();
		}
		return null;
	}

}
