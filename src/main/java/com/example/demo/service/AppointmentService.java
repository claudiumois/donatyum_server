package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.entity.Appointment;
import com.example.demo.entity.AppointmentResponse;
import com.example.demo.entity.Customer;
import com.example.demo.repository.AppointmentRepository;

@Service
@Transactional
public class AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;

	public List<Appointment> findAll() {
		return appointmentRepository.findAll();
	}

	public Optional<Appointment> findById(Long id) {
		return appointmentRepository.findById(id);
	}

	public Appointment save(Appointment appointment) {
		return appointmentRepository.save(appointment);
	}

	public void deleteById(Long id) {
		appointmentRepository.deleteById(id);
	}

	public Appointment getAppointmentById(Long id) {
		Optional<Appointment> appointment = appointmentRepository.findById(id);

		if (appointment.isPresent()) {
			return appointment.get();
		}
		return null;
	}


//	public AppointmentResponse getAllAppointmentResponse() {
//		return appointmentRepository.getAppointment();
//	}

}
