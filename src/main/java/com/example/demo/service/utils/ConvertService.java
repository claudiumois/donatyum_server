package com.example.demo.service.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.entity.Appointment;

@Component
public class ConvertService {

	@Autowired
	private ModelMapper modelMapper;
	
	public AppointmentDto convertToDto(AppointmentDto appointmentObject) {
		return modelMapper.map(appointmentObject,AppointmentDto.class);
	}
}
