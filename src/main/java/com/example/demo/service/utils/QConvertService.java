package com.example.demo.service.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.example.demo.dto.QDto;

@Component
public class QConvertService {

	@Autowired
	private ModelMapper modelMapper;
	
	public QDto convertToDto(QDto qObject) {
		return modelMapper.map(qObject,QDto.class);
	}
}
