package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Customer;
import com.example.demo.entity.StaffOcupation;
import com.example.demo.repository.StaffOcupationRepository;

@Service
@Transactional
public class StaffOcupationService {

	@Autowired
	private StaffOcupationRepository staffOcupationRepository;

	public List<StaffOcupation> findAll() {
		return staffOcupationRepository.findAll();
	}

	public Optional<StaffOcupation> findById(Long id) {
		return staffOcupationRepository.findById(id);
	}

	public StaffOcupation save(StaffOcupation staffOcupation) {
		return staffOcupationRepository.save(staffOcupation);
	}

	public void deleteById(Long id) {
		staffOcupationRepository.deleteById(id);
	}

	public StaffOcupation getStaffOcupationById(Long id) {
		Optional<StaffOcupation> staffOcupation = staffOcupationRepository.findById(id);

		if (staffOcupation.isPresent()) {
			return staffOcupation.get();
		}
		return null;
	}

}
