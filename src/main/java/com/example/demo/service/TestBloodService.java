package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.TestBlood;
import com.example.demo.repository.TestBloodRepository;


@Service
@Transactional
public class TestBloodService {

	@Autowired
	private TestBloodRepository testBloodRepository;

	public List<TestBlood> findAll() {
		return testBloodRepository.findAll();
	}

	public Optional<TestBlood> findById(Long id) {
		return testBloodRepository.findById(id);
	}

	public TestBlood save(TestBlood testBlood) {
		return testBloodRepository.save(testBlood);
	}

	public void deleteById(Long id) {
		testBloodRepository.deleteById(id);
	}

	public TestBlood getTestBloodById(Long id) {
		Optional<TestBlood> testBlood = testBloodRepository.findById(id);

		if (testBlood.isPresent()) {
			return testBlood.get();
		}
		return null;
	}

}
