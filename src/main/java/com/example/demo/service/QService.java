package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Q;
import com.example.demo.entity.Staff;
import com.example.demo.repository.QRepository;
import com.example.demo.repository.StaffRepository;

@Service
@Transactional
public class QService {

	@Autowired
	private QRepository qRepository;

	public List<Q> findAll() {
		return qRepository.findAll();
	}

	public Optional<Q> findById(Long id) {
		return qRepository.findById(id);
	}

	public Q save(Q q) {
		return qRepository.save(q);
	}

	public void deleteById(Long id) {
		qRepository.deleteById(id);
	}

	public Q getQuestionaireById(Long id) {
		Optional<Q> q = qRepository.findById(id);

		if (q.isPresent()) {
			return q.get();
		}
		return null;
	}

}
