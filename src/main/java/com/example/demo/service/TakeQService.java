package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Staff;
import com.example.demo.entity.TakeQ;
import com.example.demo.repository.StaffRepository;
import com.example.demo.repository.TakeQRepository;

@Service
@Transactional
public class TakeQService {

	@Autowired
	private TakeQRepository takeQRepository;

	public List<TakeQ> findAll() {
		return takeQRepository.findAll();
	}

	public Optional<TakeQ> findById(Long id) {
		return takeQRepository.findById(id);
	}

	public TakeQ save(TakeQ takeQ) {
		return takeQRepository.save(takeQ);
	}

	public void deleteById(Long id) {
		takeQRepository.deleteById(id);
	}

	public TakeQ getTakeQById(Long id) {
		Optional<TakeQ> takeQ = takeQRepository.findById(id);

		if (takeQ.isPresent()) {
			return takeQ.get();
		}
		return null;
	}
	
//	public TakeQ getTakeQbyCustomerID(long customerId) {/**/
//		return takeQRepository.getStatusByCustomerId(customerId);
//	}

}
