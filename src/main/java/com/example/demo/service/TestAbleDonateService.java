package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.TestAbleDonate;
import com.example.demo.repository.TestAbleDonateRepository;



@Service
@Transactional
public class TestAbleDonateService {

	@Autowired
	private TestAbleDonateRepository testAbleDonateRepository;

	public List<TestAbleDonate> findAll() {
		return testAbleDonateRepository.findAll();
	}

	public Optional<TestAbleDonate> findById(Long id) {
		return testAbleDonateRepository.findById(id);
	}

	public TestAbleDonate save(TestAbleDonate testAbleDonate) {
		return testAbleDonateRepository.save(testAbleDonate);
	}

	public void deleteById(Long id) {
		testAbleDonateRepository.deleteById(id);
	}

	public TestAbleDonate getTestAbleDonateById(Long id) {
		Optional<TestAbleDonate> testAbleDonate = testAbleDonateRepository.findById(id);

		if (testAbleDonate.isPresent()) {
			return testAbleDonate.get();
		}
		return null;
	}

}
