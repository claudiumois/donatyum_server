package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Staff;
import com.example.demo.repository.StaffRepository;

@Service
@Transactional
public class StaffService {

	@Autowired
	private StaffRepository staffRepository;
	
	List<Staff> doctors;

	public List<Staff> findAll() {
		return staffRepository.findAll();
	}

	public Optional<Staff> findById(Long id) {
		return staffRepository.findById(id);
	}

	public Staff save(Staff staff) {
		return staffRepository.save(staff);
	}

	public void deleteById(Long id) {
		staffRepository.deleteById(id);
	}

	public Staff getStaffById(Long id) {
		Optional<Staff> staff = staffRepository.findById(id);

		if (staff.isPresent()) {
			return staff.get();
		}
		return null;
	}
	
	public Staff login(String email, String password) {/**/
		return staffRepository.login(email, password);
	}

	public List<Staff> getAllDoctors() {/**/
		return staffRepository.getAllByOcupationId(2);
	}
	
	public List<Staff> getAllAdmins() {/**/
		return staffRepository.getAllByOcupationId(1);
	}
	
	public List<Staff> getAllAsistents() {/**/
		return staffRepository.getAllByOcupationId(3);
	}
}
