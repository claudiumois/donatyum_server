package com.example.demo.controller.utils;

import java.sql.Date;
import java.sql.Time;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Customer;
import com.example.demo.entity.EmailMessage;
import com.example.demo.entity.Staff;
import com.example.demo.service.CustomerService;
import com.example.demo.service.StaffService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class EmailController {

	@Value("${gmail.username}")
	private String email;

	@Value("${gmail.password}")
	private String password;

	@Autowired
	private StaffService staffService;

	@Autowired
	private CustomerService customerService;

	@PostMapping("/sendMailToDoctors/{name}/{date}/{time}")
	public String sendEmail(@PathVariable String name, @PathVariable Date date, @PathVariable Time time)
			throws MessagingException {

		List<Staff> allDoctors = staffService.getAllDoctors();/* Toti doctori */
		for (int i = 0; i < allDoctors.size(); i++) {
			EmailMessage emailmessage = new EmailMessage();
			emailmessage.setSubject("Programare pentru donare sange");
			emailmessage.setBody("Salut, aveti o noua programare pentru donare de sange facuta de " + name + "."
					+ " Data programari " + date + "\n" + ", Ora " + time);
			emailmessage.setTo_address(allDoctors.get(i).getEmail());
			sendmail(emailmessage);
		}

		return "Email send successfully";
	}

	@PostMapping("/sendMailToCustomer/{idCustomer}/{date}/{time}")
	public String sendEmailConfirmationAppointment(@PathVariable long idCustomer, @PathVariable Date date,
			@PathVariable Time time) throws MessagingException {
		Customer customer = customerService.getCustomerById(idCustomer);/* Imi gaseste customerul cu id-ul respectiv */

		if (customer != null) {
			EmailMessage emailmessage = new EmailMessage();
			emailmessage.setSubject("Confirmare programare");
			emailmessage.setBody("Salut, programarea pentru donare sange, la data " + date + " si ora " + time
					+ " a fost acceptata. Cu respect, echipa Donatyum.");
			emailmessage.setTo_address(customer.getEmail());
			sendmail(emailmessage);

			return "Email send successfully";
		} else {
			return "Email not send";
		}

	}

	@PostMapping("/sendMailForRejectAppointment/ToCustomer/{idCustomer}")
	public String sendEmailRejectAppointment(@PathVariable long idCustomer) throws MessagingException {
		Customer customer = customerService.getCustomerById(idCustomer);/* Imi gaseste customerul cu id-ul respectiv */

		if (customer != null) {
			EmailMessage emailmessage = new EmailMessage();
			emailmessage.setSubject("Reject programare");
			emailmessage.setBody(
					"Salut, ne pare rau, dar in urma completarii chestionarului facut de tine, nu esti apt pentru a dona sange."
							+ " Cu respect, echipa Donatyum.");
			emailmessage.setTo_address(customer.getEmail());
			sendmail(emailmessage);

			return "Email send successfully";
		} else {
			return "Email not send";
		}

	}

	/*
	 * Trimite mail la toti doctori cu privire la analiza donatorului
	 */
	@PostMapping("/sendMailToDoctorsWithAnaliza/{name}/{typeblood}/{bodyTemperature}/{glicemie}/{height}/{weight}/{hemoglobina}/{tension}")
	public String sendEmailToDoctorWithPredonation(@PathVariable String name, @PathVariable String typeblood,
			@PathVariable float bodyTemperature, @PathVariable float glicemie, @PathVariable float height,
			@PathVariable float weight, @PathVariable float hemoglobina, @PathVariable float tension)
			throws MessagingException {

		String titlu = "REZULTATE ANALIZE";
		String tipsange = "Tip Sange:";
		String temperaturaC = "Temperatura corpului:";
		String glic = "Glicemie:";
		String inalt = "Inaltime:";
		String greu = "Greutate:";
		String hemoglo = "Hemoglobina:";
		String tens = "Tensiune:";

		String analiza = titlu + "<br />" + tipsange + "&nbsp;&nbsp;&nbsp;&nbsp;" + typeblood + "<br />" + temperaturaC
				+ "&nbsp;&nbsp;&nbsp;&nbsp;" + typeblood + "<br />" + glic + "&nbsp;&nbsp;&nbsp;&nbsp;" + glicemie
				+ "<br />" + inalt + "&nbsp;&nbsp;&nbsp;&nbsp;" + height + "<br />" + greu + "&nbsp;&nbsp;&nbsp;&nbsp;"
				+ weight + "<br />" + hemoglo + "&nbsp;&nbsp;&nbsp;&nbsp;" + hemoglobina + "<br />" + tens
				+ "&nbsp;&nbsp;&nbsp;&nbsp;" + tension + "<br />";

		List<Staff> allDoctors = staffService.getAllDoctors();/* Toti doctori */
		for (int i = 0; i < allDoctors.size(); i++) {
			EmailMessage emailmessage = new EmailMessage();
			emailmessage.setSubject("Analiza pre-donare " + name);
			emailmessage.setBody(analiza);
			emailmessage.setTo_address(allDoctors.get(i).getEmail());
			sendmail(emailmessage);
		}

		return "Email send successfully";
	}

	/*
	 * Trimite mail la doctori cu privire la finalizarea si aparitia rezultatelor la
	 * un anumit customer
	 */
	@PostMapping("/sendMailToDoctorsForFinalAnalize/{idCustomer}")
	public String sendEmailForFinalAnalize(@PathVariable long idCustomer) throws MessagingException {
		Customer customer = customerService.getCustomerById(idCustomer);/* Imi gaseste customerul cu id-ul respectiv */

		List<Staff> allDoctors = staffService.getAllDoctors();/* Toti doctori */
		for (int i = 0; i < allDoctors.size(); i++) {
			EmailMessage emailmessage = new EmailMessage();
			emailmessage
					.setSubject("Rezultate analize finale " + customer.getFirstName() + " " + customer.getLastName());
			emailmessage.setBody("Salut, rezultatul analizelor este disponibil pe Analize finale");
			emailmessage.setTo_address(allDoctors.get(i).getEmail());
			sendmail(emailmessage);

			return "Email send successfully";
		}
		return email;

	}

	/*
	 * Trimite mail la donatori cu privire la rezultatul analizelor
	 */
	@PostMapping("/sendMailToCustomerWithFinalAnalize/{idCustomer}/{hiv}/{hbs}/{hcv}/{htlv}/{alt}/{got}/{gpt}")
	public String sendMailToCustomerWithFinalAnalize(@PathVariable long idCustomer, @PathVariable boolean hiv,
			@PathVariable boolean hbs, @PathVariable boolean hcv, @PathVariable boolean htlv, @PathVariable boolean alt,
			@PathVariable boolean got, @PathVariable boolean gpt) throws MessagingException {
		Customer customer = customerService.getCustomerById(idCustomer);/* Imi gaseste customerul cu id-ul respectiv */

		String titlu = "REZULTATE ANALIZE FINALE";
		String hivB = "HIV: ";
		String hbsB = "HBS: ";
		String hcvB = "HCV: ";
		String htlvB = "HTLV: ";
		String altB = "ALT: ";
		String gotB = "GOT: ";
		String gptB = "GPT: ";

		String analiza = titlu + "<br />" + hivB + "&nbsp;&nbsp;&nbsp;&nbsp;" + hiv + "<br />" + hbsB
				+ "&nbsp;&nbsp;&nbsp;&nbsp;" + hbs + "<br />" + hcvB + "&nbsp;&nbsp;&nbsp;&nbsp;" + hcv + "<br />"
				+ htlvB + "&nbsp;&nbsp;&nbsp;&nbsp;" + htlv + "<br />" + altB + "&nbsp;&nbsp;&nbsp;&nbsp;" + alt
				+ "<br />" + gotB + "&nbsp;&nbsp;&nbsp;&nbsp;" + got + "<br />" + gptB + "&nbsp;&nbsp;&nbsp;&nbsp;"
				+ gpt + "<br />";

		if(customer != null) {
			EmailMessage emailmessage = new EmailMessage();
			emailmessage.setSubject("REZULTATE ANALIZE FINALE " + customer.getFirstName() + " " + customer.getLastName());
			emailmessage.setBody(analiza);
			emailmessage.setTo_address(customer.getEmail());
			sendmail(emailmessage);
			return "Email send successfully";
		}else {
			return "Email not send";
		}
		
	}

	private void sendmail(EmailMessage emailMessage) throws MessagingException {

		Properties props = new Properties();

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, password);
			}
		});

		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(email, false));

		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailMessage.getTo_address()));
		msg.setSubject(emailMessage.getSubject());
		msg.setContent(emailMessage.getBody(), "text/html");
		msg.setSentDate(new Date(0, 0, 0));

		Transport.send(msg);
	}

}
