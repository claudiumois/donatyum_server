package com.example.demo.controller;

import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.Appointment;
import com.example.demo.entity.Customer;
import com.example.demo.entity.TestAbleDonate;
import com.example.demo.service.CustomerService;
import com.example.demo.service.TestAbleDonateService;

@CrossOrigin(origins = "http://localhost:3000")
@Controller
@RequestMapping("/api/testAbleDonate")
public class TestAbleDonateController {

	@Autowired
	private TestAbleDonateService testAbleDonateService;

	@Autowired
	private CustomerService customerService;

	private final static Logger log = LoggerFactory.getLogger(StaffController.class);

	@GetMapping("/find")
	public ResponseEntity<List<TestAbleDonate>> findAll() {
		return ResponseEntity.ok(testAbleDonateService.findAll());

	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody TestAbleDonate testAbleDonate) {
		return ResponseEntity.ok(testAbleDonateService.save(testAbleDonate));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<TestAbleDonate> findById(@PathVariable Long id) {
		Optional<TestAbleDonate> testAbleDonate = testAbleDonateService.findById(id);
		if (!testAbleDonate.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(testAbleDonate.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<TestAbleDonate> update(@PathVariable Long id,
			@Valid @RequestBody TestAbleDonate testAbleDonate) {
		if (!testAbleDonateService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		testAbleDonate.setId(id.intValue());
		return ResponseEntity.ok(testAbleDonateService.save(testAbleDonate));
	}

	@DeleteMapping("/delete/{id}")
	public String deleteTestAbleDonate(Model model, @PathVariable(name = "id") long id) {
		testAbleDonateService.deleteById(id);
		return "redirect:/pacient/home";
	}

	/****************************************************************************************************************************************/
	/*
	 * Creaza analiza si o asigneazza donatorului respectiv
	 * 
	 */
	@PostMapping("/setAnalizaToCustomer/{idCustomer}/{typeblood}/{bodyTemperature}/{glicemie}/{height}/{weight}/{hemoglobina}/{tension}")
	public ResponseEntity<TestAbleDonate> setAppointmentToCustomers(@PathVariable Long idCustomer,
			@PathVariable String typeblood, @PathVariable float bodyTemperature, @PathVariable float glicemie,
			@PathVariable float height, @PathVariable float weight, @PathVariable float hemoglobina,
			@PathVariable float tension) {

		Customer customer = customerService.getCustomerById(idCustomer); /* Gasit */

		if (customer != null) {/* Daca acesta exista */
			TestAbleDonate test = new TestAbleDonate();
			test.setTypeBlood(typeblood);
			test.setBodyTemperature(bodyTemperature);
			test.setGlicemie(glicemie);
			test.setHeight(height);
			test.setWeight(weight);
			test.setHemoglobina(hemoglobina);
			test.setTension(tension);
			test.setCustomer(customer);

			testAbleDonateService.save(test);
			return ResponseEntity.ok(test);
		}else {
			TestAbleDonate testDefault = new TestAbleDonate();
			testDefault.setBodyTemperature(0);
			
			return ResponseEntity.ok(testDefault);
		}
		
	}
	
	/*
	 * Returneaza analiza donatorului de la predonare
	 */
	@GetMapping("/findTestAnaliza/{idCustomer}")
	public ResponseEntity<TestAbleDonate> findTestPredonationById(@PathVariable Long idCustomer) {
		TestAbleDonate test = new TestAbleDonate();
		
		List<TestAbleDonate> allTestAbleDonate = testAbleDonateService.findAll();
		
		for(int i=0; i<allTestAbleDonate.size();i++) {
			if(allTestAbleDonate.get(i).getCustomer().getId() == idCustomer) {
				test=allTestAbleDonate.get(i);
				return ResponseEntity.ok(test);
			}
		}
		

		return null;
	}
	

}