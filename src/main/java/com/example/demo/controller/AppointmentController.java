package com.example.demo.controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.validation.Valid;
import javax.xml.ws.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.controller.utils.EmailController;
import com.example.demo.dto.AppointmentDto;
import com.example.demo.entity.Appointment;
import com.example.demo.entity.AppointmentResponse;
import com.example.demo.entity.Customer;
import com.example.demo.entity.EmailMessage;
import com.example.demo.entity.LoginModel;
import com.example.demo.entity.Staff;
import com.example.demo.service.AppointmentService;
import com.example.demo.service.CustomerService;
import com.example.demo.service.StaffService;
import com.example.demo.service.utils.ConvertService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.x.protobuf.MysqlxDatatypes.Array;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/appointment")
public class AppointmentController {

	@Autowired
	private AppointmentService appointmentService;

	@Autowired
	private ConvertService converterService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private StaffService staffService;

	private final static Logger log = LoggerFactory.getLogger(StaffController.class);

	@GetMapping("/find")
	public ResponseEntity<List<Appointment>> findAll() {
		return ResponseEntity.ok(appointmentService.findAll());
	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Appointment appointment) throws MessagingException {
		return ResponseEntity.ok(appointmentService.save(appointment));
	}

	@PostMapping("/createAppointmentWithCustomer")
	public ResponseEntity createAppointmentWithCustomer(@Valid @RequestBody Appointment appointment,
			@RequestBody Customer customer) {
		return ResponseEntity.ok(appointmentService.save(appointment));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Appointment> findById(@PathVariable Long id) {
		Optional<Appointment> appointment = appointmentService.findById(id);
		if (!appointment.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(appointment.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Appointment> update(@PathVariable Long id, @Valid @RequestBody Appointment appointment) {
		if (!appointmentService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}
		appointment.setIdAppointment(id.intValue());
		return ResponseEntity.ok(appointmentService.save(appointment));
	}

	@DeleteMapping("/delete/{id}")
	public String deleteAppointment(Model model, @PathVariable(name = "id") long id) {
		appointmentService.deleteById(id);
		return "redirect:/staff/home";
	}

	/***************************************************
	 * START
	 ********************************************************/

	/*
	 * Creaza o programare la un donator cu datele trimise in request
	 * 
	 * 
	 * http://localhost:8080/api/appointment/setAppointmentToCustomer/2/2022-07-18/
	 * 17:30:00
	 */
	@PostMapping("/setAppointmentToCustomer/{idCustomer}/{date}/{time}")
	public ResponseEntity<Appointment> setAppointmentToCustomers(@PathVariable Long idCustomer, @PathVariable Date date,
			@PathVariable Time time) {

		Customer customer = customerService.getCustomerById(idCustomer); /* Gasit */

		Appointment appointment = new Appointment();
		appointment.setDate(date);
		appointment.setTime(time);
		appointment.setCustomer(customer);
		appointmentService.save(appointment);
		return ResponseEntity.ok(appointment);
	}

	/*
	 * Imi returneaza programarea a fiecarui donator
	 * 
	 * http://localhost:8080/api/appointment/getAppointmentByCustomerId/3
	 * 
	 */
	@GetMapping("/getAppointmentByCustomerId/{customerId}")
	public AppointmentDto getAppointmentToCustomer(@PathVariable Long customerId) {
		List<Appointment> allAppointments = appointmentService.findAll();
		AppointmentDto appointmentDTO = new AppointmentDto();
		for (int i = 0; i < allAppointments.size(); i++) {
			if (allAppointments.get(i).getCustomer().getIdCustomer() == customerId) {
				appointmentDTO.setId(allAppointments.get(i).getIdAppointment());
				appointmentDTO.setDate(allAppointments.get(i).getDate());
				appointmentDTO.setTime(allAppointments.get(i).getTime());
				appointmentDTO.setFirstName(allAppointments.get(i).getCustomer().getFirstName());
				appointmentDTO.setLastName(allAppointments.get(i).getCustomer().getLastName());
			}
		}
		return appointmentDTO;
	}

	/*
	 * Trebe sa-mi returneze o lista cu detalii despre donator cu programare
	 * 
	 * http://localhost:8080/api/appointment/getAllAppointments
	 * 
	 */
	@RequestMapping("/getAllAppointments")
	public List<AppointmentDto> setAppointmentToCustomer() throws IOException {
		List<Customer> allCustomers = customerService.findAll();
		List<Appointment> allAppointments = appointmentService.findAll();
		List<AppointmentDto> result = new ArrayList<>();
		for (int i = 0; i < allAppointments.size(); i++) {
			Customer customer = customerService.getCustomerById(allAppointments.get(i).getCustomer().getIdCustomer());
			AppointmentDto customerWithappointment = new AppointmentDto();
			if (allAppointments.get(i).getCustomer() != null) {
				customerWithappointment.setId(allAppointments.get(i).getIdAppointment());
				customerWithappointment.setIdCustomer(customer.getIdCustomer());
				customerWithappointment.setDate(allAppointments.get(i).getDate());
				customerWithappointment.setTime(allAppointments.get(i).getTime());
				customerWithappointment.setFirstName(allAppointments.get(i).getCustomer().getFirstName());
				customerWithappointment.setLastName(allAppointments.get(i).getCustomer().getLastName());
				customerWithappointment.setStatus(allAppointments.get(i).getStatus());
				result.add(customerWithappointment);
			}
		}
		return result.stream().map(converterService::convertToDto).collect(Collectors.toList());
	}

	/*
	 * Metoda care va fi apelata de doctor cand modifica statusul programarii adica
	 * daca accespta programarea pe data si ora respectiva, introdusa de client
	 */
	@PutMapping("/updateAppointment/{idCustomer}/accept")
	public ResponseEntity<Appointment> updateStatusAppointment(@PathVariable Long idCustomer) {

		Customer customer = customerService.getCustomerById(idCustomer);

		Appointment appointment = customer.getAppointment();
		appointment.setStatus(1);

		return ResponseEntity.ok(appointmentService.save(appointment));
	}

	/*
	 * Returneaza toate programarile care nu au fost acceptate inca de doctor(sau
	 * verificate) de catre doctor(care au status 1)
	 * 
	 */
	@GetMapping("/findAllAppointmentsNoChecked")
	public ResponseEntity<List<Appointment>> findAllAppointmentNoChecked() {
		List<Appointment> allAppointment = appointmentService.findAll();
		List<Appointment> allAppointmentWithStatusZero = new ArrayList<Appointment>();

		for (int i = 0; i < allAppointment.size(); i++) {
			if (allAppointment.get(i).getStatus() == 0) {
				allAppointmentWithStatusZero.add(allAppointment.get(i));
			}
		}

		return ResponseEntity.ok(allAppointmentWithStatusZero);
	}

	/*
	 * Returneaza toate programarile care sunt verificate de catre doctor(in functie
	 * de ora si data si chestionar)
	 */
	@RequestMapping("/findAllAppointmentsChecked")
	public List<AppointmentDto> getAppointmentsChecked() throws IOException {
		List<Customer> allCustomers = customerService.findAll();
		List<Appointment> allAppointments = appointmentService.findAll();
		List<AppointmentDto> result = new ArrayList<>();
		for (int i = 0; i < allAppointments.size(); i++) {
			Customer customer = customerService.getCustomerById(allAppointments.get(i).getCustomer().getIdCustomer());
			AppointmentDto customerWithappointment = new AppointmentDto();
			if (allAppointments.get(i).getCustomer() != null && allAppointments.get(i).getStatus() == 1) {
				customerWithappointment.setId(allAppointments.get(i).getIdAppointment());
				customerWithappointment.setIdCustomer(customer.getIdCustomer());
				customerWithappointment.setDate(allAppointments.get(i).getDate());
				customerWithappointment.setTime(allAppointments.get(i).getTime());
				customerWithappointment.setFirstName(allAppointments.get(i).getCustomer().getFirstName());
				customerWithappointment.setLastName(allAppointments.get(i).getCustomer().getLastName());
				customerWithappointment.setStatus(allAppointments.get(i).getStatus());
				result.add(customerWithappointment);
			}
		}
		return result.stream().map(converterService::convertToDto).collect(Collectors.toList());
	}
	
	/*
	 * Returneaza toate donarile cu status 0
	 */
	@RequestMapping("/getAllAppointmentsWithStatusZero")
	public List<AppointmentDto> setAppointmentNoCheckedToCustomer() throws IOException {
		List<Customer> allCustomers = customerService.findAll();
		List<Appointment> allAppointments = appointmentService.findAll();
		List<AppointmentDto> result = new ArrayList<>();
		for (int i = 0; i < allAppointments.size(); i++) {
			Customer customer = customerService.getCustomerById(allAppointments.get(i).getCustomer().getIdCustomer());
			AppointmentDto customerWithappointment = new AppointmentDto();
			if (allAppointments.get(i).getCustomer() != null && allAppointments.get(i).getStatus()==0) {
				customerWithappointment.setId(allAppointments.get(i).getIdAppointment());
				customerWithappointment.setIdCustomer(customer.getIdCustomer());
				customerWithappointment.setDate(allAppointments.get(i).getDate());
				customerWithappointment.setTime(allAppointments.get(i).getTime());
				customerWithappointment.setFirstName(allAppointments.get(i).getCustomer().getFirstName());
				customerWithappointment.setLastName(allAppointments.get(i).getCustomer().getLastName());
				customerWithappointment.setStatus(allAppointments.get(i).getStatus());
				result.add(customerWithappointment);
			}
		}
		return result.stream().map(converterService::convertToDto).collect(Collectors.toList());
	}
	
	@RequestMapping("/getAllAppointmentsWithStatusUnu")
	public List<AppointmentDto> setAppointmentCheckedToCustomer() throws IOException {
		List<Customer> allCustomers = customerService.findAll();
		List<Appointment> allAppointments = appointmentService.findAll();
		List<AppointmentDto> result = new ArrayList<>();
		for (int i = 0; i < allAppointments.size(); i++) {
			Customer customer = customerService.getCustomerById(allAppointments.get(i).getCustomer().getIdCustomer());
			AppointmentDto customerWithappointment = new AppointmentDto();
			if (allAppointments.get(i).getCustomer() != null && allAppointments.get(i).getStatus()==1) {
				customerWithappointment.setId(allAppointments.get(i).getIdAppointment());
				customerWithappointment.setIdCustomer(customer.getIdCustomer());
				customerWithappointment.setDate(allAppointments.get(i).getDate());
				customerWithappointment.setTime(allAppointments.get(i).getTime());
				customerWithappointment.setFirstName(allAppointments.get(i).getCustomer().getFirstName());
				customerWithappointment.setLastName(allAppointments.get(i).getCustomer().getLastName());
				customerWithappointment.setStatus(allAppointments.get(i).getStatus());
				result.add(customerWithappointment);
			}
		}
		return result.stream().map(converterService::convertToDto).collect(Collectors.toList());
	}

}