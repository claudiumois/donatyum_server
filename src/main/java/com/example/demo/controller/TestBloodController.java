package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.Customer;
import com.example.demo.entity.TestAbleDonate;
import com.example.demo.entity.TestBlood;
import com.example.demo.service.CustomerService;
import com.example.demo.service.TestBloodService;

@CrossOrigin(origins = "http://localhost:3000")
@Controller
@RequestMapping("/api/testBlood")
public class TestBloodController {

	@Autowired
	private TestBloodService testBloodService;
	
	@Autowired
	private CustomerService customerService;

	private final static Logger log = LoggerFactory.getLogger(CustomerController.class);

	@GetMapping("/find")
	public ResponseEntity<List<TestBlood>> findAll() {
		return ResponseEntity.ok(testBloodService.findAll());

	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody TestBlood testBlood) {
		return ResponseEntity.ok(testBloodService.save(testBlood));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<TestBlood> findById(@PathVariable Long id) {
		Optional<TestBlood> testBlood = testBloodService.findById(id);
		if (!testBlood.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(testBlood.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<TestBlood> update(@PathVariable Long id, @Valid @RequestBody TestBlood testBlood) {
		if (!testBloodService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		testBlood.setId(id.intValue());
		return ResponseEntity.ok(testBloodService.save(testBlood));
	}

	@RequestMapping("/delete/{id}")
	public String deleteDonor(Model model, @PathVariable(name = "id") long id) {
		testBloodService.deleteById(id);
		return "redirect:/pacient/home";
	}/* Trebe sa-i dau eu alt path aici */
	
	
	/****************************************************************************************************************************************/
	/*
	 * Creaza analiza finala si o asigneazza donatorului respectiv
	 * 
	 */
	@PostMapping("/setAnalizaFinalaToCustomer/{idCustomer}/{hiv}/{htlv}/{hbs}/{hcv}/{alt}/{got}/{gpt}")
	public ResponseEntity<TestBlood> setFinalAnalizaToCustomers(@PathVariable Long idCustomer,
			@PathVariable boolean hiv, @PathVariable boolean htlv, @PathVariable boolean hbs,
			@PathVariable boolean hcv, @PathVariable boolean alt, @PathVariable boolean got,
			@PathVariable boolean gpt) {

		Customer customer = customerService.getCustomerById(idCustomer); /* Gasit */

		if (customer != null) {/* Daca acesta exista */
			TestBlood test = new TestBlood();
			test.setAlt(alt);
			test.setGot(got);
			test.setGpt(gpt);
			test.setHbs(hbs);
			test.setHcv(hcv);
			test.setHtlv(htlv);
			test.setHiv(hiv);
			test.setCustomer(customer);

			testBloodService.save(test);
			return ResponseEntity.ok(test);
		}else {
			return null;
		}
		
	}
	
	/*
	 * Returneaza analiza donatorului de la compartimentul de donare
	 */
	@GetMapping("/findTestAnaliza/{idCustomer}")
	public ResponseEntity<TestBlood> findFinalTestCompartimentById(@PathVariable Long idCustomer) {
		TestBlood test = new TestBlood();
		
		List<TestBlood> allTestBloods = testBloodService.findAll();
		
		for(int i=0; i<allTestBloods.size();i++) {
			if(allTestBloods.get(i).getCustomer().getId() == idCustomer) {
				test=allTestBloods.get(i);
				return ResponseEntity.ok(test);
			}
		}
		return null;
	}
	

}