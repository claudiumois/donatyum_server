package com.example.demo.controller;

import java.util.List;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Customer;
import com.example.demo.entity.LoginModel;
import com.example.demo.entity.Staff;
import com.example.demo.service.CustomerService;
import com.example.demo.service.StaffService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private StaffService staffService;

	private final static Logger log = LoggerFactory.getLogger(CustomerController.class);

	@GetMapping("/find")
	public ResponseEntity<List<Customer>> findAll() {
		return ResponseEntity.ok(customerService.findAll());
	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Customer customer) {
		return ResponseEntity.ok(customerService.save(customer));
	}

	@PostMapping("/login")
	public ResponseEntity<LoginModel> loginModel(@RequestBody LoginModel loginModel) {
		LoginModel user = new LoginModel();

		Customer userOn = customerService.login(loginModel.getEmail(), loginModel.getPassword());

		Staff staffOn = staffService.login(loginModel.getEmail(), loginModel.getPassword());

		if (userOn != null) {
			user.setUserId(userOn.getIdCustomer());
			user.setEmail(userOn.getEmail());
			user.setPassword(userOn.getPassword());
			user.setUserOcupationId(0);
		} else if (staffOn != null) {
			user.setUserId(staffOn.getIdStaff());
			user.setEmail(staffOn.getEmail());
			user.setPassword(staffOn.getPassword());
			user.setUserOcupationId(staffOn.getStaffOcupationId());
		}

		return ResponseEntity.ok(user);
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Customer> findById(@PathVariable Long id) {
		Optional<Customer> customer = customerService.findById(id);
		if (!customer.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(customer.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Customer> update(@PathVariable Long id, @Valid @RequestBody Customer customer) {
		if (!customerService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		customer.setIdCustomer(id.intValue());
		return ResponseEntity.ok(customerService.save(customer));
	}

	@RequestMapping("/delete/{id}")
	public String deleteCustomer(Model model, @PathVariable(name = "id") long id) {
		customerService.deleteById(id);
		return "redirect:/pacient/home";
	}/* Trebe sa-i dau eu alt path aici */

}