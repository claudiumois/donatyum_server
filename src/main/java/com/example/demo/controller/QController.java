package com.example.demo.controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.dto.QDto;
import com.example.demo.entity.Appointment;
import com.example.demo.entity.Customer;
import com.example.demo.entity.Q;
import com.example.demo.service.CustomerService;
import com.example.demo.service.QService;
import com.example.demo.service.utils.QConvertService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/q")
public class QController {

	@Autowired
	private QService qService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private QConvertService qConvertService;

	private final static Logger log = LoggerFactory.getLogger(QController.class);

	@GetMapping("/find")
	public ResponseEntity<List<Q>> findAll() {
		return ResponseEntity.ok(qService.findAll());
	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Q q) {
		return ResponseEntity.ok(qService.save(q));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Q> findById(@PathVariable Long id) {
		Optional<Q> q = qService.findById(id);
		if (!q.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(q.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Q> update(@PathVariable Long id, @Valid @RequestBody Q q) {
		if (!qService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		q.setId(id.intValue());
		return ResponseEntity.ok(qService.save(q));
	}

	@DeleteMapping("/delete/{id}")
	public String deleteQuestionaire(Model model, @PathVariable(name = "id") long id) {
		qService.deleteById(id);
		return "redirect:/q/home";
	}

	/*****************************************
	 * START
	 ********************************************/

	/*
	 * Imi returneaza chestionarul cu toate intrebarile si raspunsurile date de un donator dupa id
	 * 
	 * 
	 */
	@GetMapping("/getAllQ/byCustomer/{idCustomer}")
	public List<QDto> getAllQByCustomer(@PathVariable(name = "idCustomer") long idCustomer) throws IOException {
		Customer customer = customerService.getCustomerById(idCustomer); /* Am gasit donatorul daca exista */
		List<Q> allQ = qService.findAll();/* Am gasit toate intrebarile de la toti donatorii */
		List<QDto> allQByCustomer = new ArrayList<>();

		if (customer != null) {
			for (int i = 0; i < allQ.size(); i++) {

				if (allQ.get(i).getCustomer().getId() == idCustomer) {

					QDto qDto = new QDto();
					qDto.setId(allQ.get(i).getId());
					qDto.setAnswer_a(allQ.get(i).getAnswer_a());
					qDto.setAnswer_b(allQ.get(i).getAnswer_b());
					qDto.setAnswerCustomer(allQ.get(i).getAnswerCustomer());
					qDto.setIdCustomer(allQ.get(i).getCustomer().getId());
					qDto.setQuestion(allQ.get(i).getQuestion());

					allQByCustomer.add(qDto);
				}
			}
		}

		return allQByCustomer.stream().map(qConvertService::convertToDto).collect(Collectors.toList());
	}
	
	/*Creaza o intrebare cu raspuns la un donator cu datele trimise in request
	 * 
	 * Daca e o problema e posibil din request la "?"
	 * 
	 * http://localhost:8080/api/appointment/setAppointmentToCustomer/2/2022-07-18/17:30:00
	 * */
	@PostMapping("/setQToCustomer/{idCustomer}/{question}/{answer_a}/{answer_b}/{answerCustomer}")
	public ResponseEntity<Q> setQToCustomer(@PathVariable Long idCustomer, @PathVariable String question, @PathVariable String answer_a, 
			@PathVariable String answer_b, @PathVariable String answerCustomer) {
		
		Customer customer = customerService.getCustomerById(idCustomer); /*Gasit*/
		Q q = new Q();/*creare obiect pentru intrebare cu raspuns*/
		q.setQuestion(question);
		q.setAnswer_a(answer_a);
		q.setAnswer_b(answer_b);
		q.setAnswerCustomer(answerCustomer);
		q.setCustomer(customer);
		qService.save(q);
		
		return ResponseEntity.ok(q);
	}

}