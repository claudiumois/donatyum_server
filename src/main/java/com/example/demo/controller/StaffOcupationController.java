package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.StaffOcupation;
import com.example.demo.service.StaffOcupationService;

@CrossOrigin(origins = "http://localhost:3000")
@Controller
@RequestMapping("/api/staffOcupation")
public class StaffOcupationController {

	@Autowired
	private StaffOcupationService staffOcupationService;

	private final static Logger log = LoggerFactory.getLogger(StaffOcupationController.class);

	@GetMapping("/find")
	public ResponseEntity<List<StaffOcupation>> findAll() {
		return ResponseEntity.ok(staffOcupationService.findAll());

	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody StaffOcupation staffOcupation) {
		return ResponseEntity.ok(staffOcupationService.save(staffOcupation));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<StaffOcupation> findById(@PathVariable Long id) {
		Optional<StaffOcupation> staffOcupation = staffOcupationService.findById(id);
		if (!staffOcupation.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(staffOcupation.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<StaffOcupation> update(@PathVariable Long id, @Valid @RequestBody StaffOcupation staffOcupation) {
		if (!staffOcupationService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		staffOcupation.setIdStaffOcupation(id.intValue());
		return ResponseEntity.ok(staffOcupationService.save(staffOcupation));
	}

	@DeleteMapping("/delete/{id}")
	public String deleteStaffOcupation(Model model, @PathVariable(name = "id") long id) {
		staffOcupationService.deleteById(id);
		return "redirect:/staffOcupation/home";
	}

}