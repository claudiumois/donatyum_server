package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;
import java.util.Optional;

import javax.persistence.Entity;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entity.Staff;
import com.example.demo.service.StaffService;

@CrossOrigin(origins = "http://localhost:3000")
@Controller
@RequestMapping("/api/staff")
public class StaffController {

	@Autowired
	private StaffService staffService;

	private final static Logger log = LoggerFactory.getLogger(StaffController.class);

	@GetMapping("/find")
	public ResponseEntity<List<Staff>> findAll() {
		return ResponseEntity.ok(staffService.findAll());
	}
	
	@GetMapping("/doctors/find")
	public ResponseEntity<List<Staff>> findAllDoctor() {
		return ResponseEntity.ok(staffService.getAllDoctors());
	}
	
	@GetMapping("/admins/find")
	public ResponseEntity<List<Staff>> findAllAdmin() {
		return ResponseEntity.ok(staffService.getAllAdmins());
	}
	
	@GetMapping("/asistents/find")
	public ResponseEntity<List<Staff>> findAllAsistents() {
		return ResponseEntity.ok(staffService.getAllAsistents());
	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Staff staff) {
		return ResponseEntity.ok(staffService.save(staff));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Staff> findById(@PathVariable Long id) {
		Optional<Staff> staff = staffService.findById(id);
		if (!staff.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(staff.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Staff> update(@PathVariable Long id, @Valid @RequestBody Staff staff) {
		if (!staffService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		staff.setIdStaff(id.intValue());
		return ResponseEntity.ok(staffService.save(staff));
	}

	@DeleteMapping("/delete/{id}")
	public String deleteStaff(Model model, @PathVariable(name = "id") long id) {
		staffService.deleteById(id);
		return "redirect:/staff/home";
	}

}