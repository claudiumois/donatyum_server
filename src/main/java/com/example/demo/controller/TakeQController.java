package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Q;
import com.example.demo.entity.Staff;
import com.example.demo.entity.TakeQ;
import com.example.demo.service.CustomerService;
import com.example.demo.service.QService;
import com.example.demo.service.StaffService;
import com.example.demo.service.TakeQService;

@CrossOrigin(origins = "http://localhost:3000")
@Controller
@RequestMapping("/api/takeq")
public class TakeQController {

	@Autowired
	private TakeQService takeQService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private QService qService;

	private final static Logger log = LoggerFactory.getLogger(TakeQService.class);

	@GetMapping("/find")
	public ResponseEntity<List<TakeQ>> findAll() {
		return ResponseEntity.ok(takeQService.findAll());

	}
	
	@GetMapping("/checkifexistcompleteq/{idCustomer}")
	public ResponseEntity<TakeQ> getTakeQByCustomer(@PathVariable Long idCustomer) {
		
		List<TakeQ> allCheck = takeQService.findAll();
		TakeQ takeq = new TakeQ();
		
		for(int i=0;i<allCheck.size();i++) {
			
			if(allCheck.get(i).getCustomerId() == idCustomer) {
				takeq.setCustomerId(allCheck.get(i).getCustomerId());
				takeq.setIdTakeQ(allCheck.get(i).getIdTakeQ());
				takeq.setStatus(allCheck.get(i).getStatus());
				
				
			}
			
		}
		return ResponseEntity.ok(takeq);
		
		

	}
	

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody TakeQ takeQ) {
		return ResponseEntity.ok(takeQService.save(takeQ));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<TakeQ> findById(@PathVariable Long id) {
		Optional<TakeQ> takeQ = takeQService.findById(id);
		if (!takeQ.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(takeQ.get());
	}

	@DeleteMapping("/delete/{id}")
	public String deleteTakeQ(Model model, @PathVariable(name = "id") long id) {
		takeQService.deleteById(id);
		return "redirect:/takeq/home";
	}

}