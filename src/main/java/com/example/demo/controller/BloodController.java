package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Blood;
import com.example.demo.entity.Customer;
import com.example.demo.entity.TestAbleDonate;
import com.example.demo.entity.TestBlood;
import com.example.demo.service.BloodService;
import com.example.demo.service.CustomerService;
import com.example.demo.service.TestBloodService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/blood")
public class BloodController {

	@Autowired
	private BloodService bloodService;
	
	@Autowired
	private CustomerService customerService;

	private final static Logger log = LoggerFactory.getLogger(CustomerController.class);

	@GetMapping("/find")
	public ResponseEntity<List<Blood>> findAll() {
		return ResponseEntity.ok(bloodService.findAll());

	}

	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Blood  blood) {
		return ResponseEntity.ok(bloodService.save(blood));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Blood> findById(@PathVariable Long id) {
		Optional<Blood> blood = bloodService.findById(id);
		if (!blood.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(blood.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Blood> update(@PathVariable Long id, @Valid @RequestBody Blood blood) {
		if (!bloodService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		blood.setId(id.intValue());
		return ResponseEntity.ok(bloodService.save(blood));
	}

	@RequestMapping("/delete/{id}")
	public String deleteDonor(Model model, @PathVariable(name = "id") long id) {
		bloodService.deleteById(id);
		return "redirect:/pacient/home";
	}/* Trebe sa-i dau eu alt path aici */
	
	
	/****************************************************************************************************************************************/
	/*
	 * Se stocheaza sangele in centru si o asigneaza donatorului respectiv(al cui este sangele, de la compartimentul de donare)
	 */
	@PostMapping("/setBloodFromCustomer/{idCustomer}/{status}")
	public ResponseEntity<Blood> setAppointmentToCustomers(@PathVariable Long idCustomer, @PathVariable int status) {

		Customer customer = customerService.getCustomerById(idCustomer); /* Gasit */

		if (customer != null) {/* Daca acesta exista */
			Blood blood = new Blood();
			
			blood.setQuantity(450);
			blood.setStatus(status);
			blood.setTypeblood(customer.getTestAbleDonate().getTypeBlood());
			blood.setCustomer(customer);

			bloodService.save(blood);
			return ResponseEntity.ok(blood);
		}else {
			return ResponseEntity.ok(null);
		}
		
	}
	
	
	
	
	

}